FROM python:3.9-slim
MAINTAINER RESIF DC <resif-dc@univ-grenoble-alpes.fr>
RUN pip install --no-cache-dir gunicorn
COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt
 
WORKDIR /apps
COPY ws-resifsi .

CMD ["/bin/bash", "-c", "gunicorn --bind 0.0.0.0:8000 app:app"]
