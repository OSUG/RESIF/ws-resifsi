# -*- coding: utf-8 -*-
"""
 Declartion of flask application's configuration  
"""


class Config:

    DEBUG = True
    TESTING = False


class ProductionConfig(Config):
    DEBUG = True


class DevelopmentConfig(Config):
    TESTING = True
    ENV = "development"


class TestingConfig(Config):
    TESTING = True
    ENV = "TESTING"
