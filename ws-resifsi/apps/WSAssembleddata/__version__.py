"""
 Define version wenservice

	:param MAJOR_VERSION: Update if there is a general change in the general functioning of the application
	:param MINOR_VERSION: Update if a new feature is added 
	:param PATCH_VERSION: Update if some bug is fixed 
	
"""

MAJOR_VERSION = "1"
MINOR_VERSION = MAJOR_VERSION + ".1"
PATCH_VERSION = MINOR_VERSION + ".4"
