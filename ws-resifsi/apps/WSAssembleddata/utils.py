"""
Contain all global variables and functions to check  parameters errors.

    :param EVENTS_DIR: Path to the  events zips
    :param FORMAT_LIST: possible values of format parameter
    :param MAX_ARG_FORM1: The number of arguments allowed to get events with name 
    :param MAX_ARG_FORM2: The number of arguments allowed to get events with filter
    :param MAX_ARGUMENTS: The number of arguments allowed in the webservice assembleddata 
    :param TIMEOUT: Timeout of the connection to database
    :param URL_ARGUMENTS: List of web service arguments
"""
import os
from datetime import datetime
from .__version__ import PATCH_VERSION
from flask import send_file, request, Response, make_response, current_app
from dateutil import parser
from ..extensions.UtilErrors import error_handler
from ..extensions.UtilValidate import CheckArg



EVENTS_DIR = os.getenv("EVENTS_DIR", "/mnt/auto/archive/metadata/portalproducts/events")
MAX_ARGUMENTS = 13
MAX_ARG_FORM1 = 2
MAX_ARG_FORM2 = 11
FORMAT_LIST = ["archive", "text", "json", None]
COLLECTOR = ["RAP", "RESIF", None]
TIMEOUT = "10000"
URL_ARGUMENTS = [
    "name",
    "before",
    "after",
    "minlatitude",
    "maxlatitude",
    "collector",
    "minlongitude",
    "maxlongitude",
    "maxmagnitude",
    "minmagnitude",
    "mindepth",
    "maxdepth",
    "agency",
    "format",
]


class CheckArgAssembleddata(CheckArg):
    def isValidFormat(format):
        return format in FORMAT_LIST

    def isValidLatitude(latitude):
        """vérifier la valeur de latitude

        Args:
            latitude (TYPE): Description
            latitude (str)

        Returns:
            boolean: True/False
        """
        return float(latitude) >= -90 and float(latitude) <= 90

    def isValidLongitude(longitude):
        """Vérifier la valeur de longitude

        Args:
            longitude (TYPE): Description
            longitude (str)

        Returns:
            boolean: True/False
        """
        return float(longitude) >= -180 and float(longitude) <= 180

    def isValidDepth(depth):
        """Vérifier la valeur de profondeur

        Args:
            depth (TYPE): Description
            depth (str)

        Returns:
            boolean: True/False
        """
        # ignoré pour l'instant
        return float(depth) < 0

    def isValidCollector(collector):
        """

        Args:
            collector (str): Description
        """
        return collector in COLLECTOR

    # Gestion des erreurs
    def compare(min, max):
        """
        Comparer si le Min est bien inferieur au Max

        Args:
            min (float): la valeur min de l'url
            max (float): la valeur max de l'url

        Returns:
            booleen: True si le min est bien inferieur au max, False sinon
        """
        if min > max:
            return False
        else:
            return True

    def error_time_handler(before, after):
        """
        Traiter les erreurs des parametres de type datetime

        Args:
            before (str): Description
            after (TYPE): Description
            after (str: Description

        Returns:
            None: si tous est OK / pasd'erreurs
            Error: appel la focntion pour traiter l'erreur détecté
        """

        if before is None or after is None:
            return error_request(
                msg=Error._400_,
                msg_detail=Error.REQUIRED_TIME,
                code=400,
                version=PATCH_VERSION,
            )

        if not isValidTime(before):
            return error_request(
                msg=Error._400_,
                msg_detail=Error.INVL_TIME + before,
                code=400,
                version=PATCH_VERSION,
            )

        if not isValidTime(after):
            return error_request(
                msg=Error._400_,
                msg_detail=Error.INVL_TIME + after,
                code=400,
                version=PATCH_VERSION,
            )

        if not compare(after.replace("T", " "), before.replace("T", " ")):
            return error_request(
                msg=Error._400_,
                msg_detail=Error.BEFORE_AFTER,
                code=400,
                version=PATCH_VERSION,
            )

        if (parser.parse(before) - parser.parse(after)).days > 31:
            return error_request(
                msg=Error._403_, msg_detail=Error.MAX, code=403, version=PATCH_VERSION
            )
        return None
