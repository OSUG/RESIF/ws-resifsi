"""
The file contains the parameters for connecting to the database.

A parameters.py file in WS* must contain 
the connection parameters

"""
import os

host = os.getenv('PGHOST', 'localhost')
database = os.getenv('PGDATABASE', 'orphanfiles')
user = os.getenv('PGUSER', 'wsresifsi')
password = os.getenv('PGPASSWORD', '')
