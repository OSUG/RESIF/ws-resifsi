"""
 Create Assembleddata  namespace with  its resources.

"""

from flask_restplus import Namespace
from .resources import *
from .__version__ import MAJOR_VERSION


assembleddata_ns = Namespace("assembleddata")
assembleddata_ns.add_resource(Events, f"/{MAJOR_VERSION}/query")
assembleddata_ns.add_resource(Version, f"/{MAJOR_VERSION}/version")
assembleddata_ns.add_resource(Application, f"/{MAJOR_VERSION}/application.wadl")
assembleddata_ns.add_resource(Docs_assembleddata, f"/{MAJOR_VERSION}/", f"/")
