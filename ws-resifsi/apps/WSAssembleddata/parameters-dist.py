"""
The file contains the parameters for connecting to the database.

A parameters.py file in WS* must contain 
the connection parameters

"""

host = " Adresse de serveur"
database = " le nom de la base de données"
user = " utilisateur"
password = " mot de passe"
