"""
Define the GET, POST, DELETE functions for each resource.

"""
from flask_restplus import Resource, reqparse
from .lib import *
from .utils import *
from flask import request, current_app, render_template
from .__version__ import PATCH_VERSION
import urllib.request

from apps import logger_Assembleddata
from ..extensions.UtilErrors import error_request, Error
from .utils import CheckArgAssembleddata as Args


class Events(Resource):
    def get(self):

        """Get events
        - Retrieve the parameters of the requests
        - The management of parameter errors
        - Calling up the corresponding functions

        """
        try:

            args_request = request.args.copy()
            list_args_request = list(args_request.items(multi=True))

            if len(list_args_request) > MAX_ARGUMENTS:
                return error_request(
                    msg=Error._400_,
                    msg_detail=Error.LEN_ARGS,
                    code=400,
                    version=PATCH_VERSION,
                )
            for arg in args_request.keys():
                if arg not in URL_ARGUMENTS:
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.UNKNOWN_PARM + arg,
                        code=400,
                        version=PATCH_VERSION,
                    )

            parser = reqparse.RequestParser()
            logger_Assembleddata.debug(f" Analyse request {request.url} ")
            parser.add_argument("name", type=str)
            parser.add_argument("format", type=str)
            parser.add_argument("collector", type=str)
            parser.add_argument("after", type=str)
            parser.add_argument("before", type=str)
            parser.add_argument("minlatitude", type=str)
            parser.add_argument("maxlatitude", type=str)
            parser.add_argument("minlongitude", type=str)
            parser.add_argument("maxlongitude", type=str)
            parser.add_argument("minmagnitude", type=str)
            parser.add_argument("maxmagnitude", type=str)
            parser.add_argument("mindepth", type=str)
            parser.add_argument("maxdepth", type=str)
            parser.add_argument("agency", type=str)

            args = parser.parse_args()
            logger_Assembleddata.debug(f"Arguments in request:  {list_args_request}")

            name = args.get("name", None)
            format = args.get("format", None)
            collector = args.get("collector", None)
            after = args.get("after", None)
            before = args.get("before", None)
            minlatitude = args.get("minlatitude", None)
            maxlatitude = args.get("maxlatitude", None)
            minlongitude = args.get("minlongitude", None)
            maxlongitude = args.get("maxlongitude", None)
            minmagnitude = args.get("minmagnitude", None)
            maxmagnitude = args.get("maxmagnitude", None)
            mindepth = args.get("mindepth", None)
            maxdepth = args.get("maxdepth", None)
            agency = args.get("agency", None)

            if format != None and not Args.isValidFormat(format):
                return error_request(
                    msg=Error._400_,
                    msg_detail=f"{Error.INVL_FORMAT} {format}",
                    code=400,
                    version=PATCH_VERSION,
                )

            if name is not None:
                parameters = [key for key in args_request.keys()]
                if (
                    len(list_args_request) > MAX_ARG_FORM1
                    and "format" not in parameters
                ):
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.LEN_ARGS_FORM1,
                        code=400,
                        version=PATCH_VERSION,
                    )

                return getEventsArchivByName(name, format)

            else:

                result = error_time_handler(before, after)
                if result is not None:
                    return result

                if maxlatitude is not None and not Args.isValidLatitude(maxlatitude):
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.INVL_LATITUDE,
                        code=400,
                        version=PATCH_VERSION,
                    )

                if minlatitude is not None and not Args.isValidLatitude(minlatitude):
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.INVL_LATITUDE,
                        code=400,
                        version=PATCH_VERSION,
                    )

                if (
                    minlatitude is not None
                    and maxlatitude is not None
                    and not compare(float(minlatitude), float(maxlatitude))
                ):
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.MAX_MIN_LATITUDE,
                        code=400,
                        version=PATCH_VERSION,
                    )

                if maxlongitude is not None and not Args.isValidLongitude(maxlongitude):
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.INVL_LONGITUDE,
                        code=400,
                        version=PATCH_VERSION,
                    )

                if minlongitude is not None and not Args.isValidLongitude(maxlongitude):
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.INVL_LONGITUDE,
                        code=400,
                        version=PATCH_VERSION,
                    )

                if (
                    minlongitude is not None
                    and maxlongitude is not None
                    and not compare(float(minlongitude), float(maxlongitude))
                ):
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.MAX_MIN_LONGITUDE,
                        code=400,
                        version=PATCH_VERSION,
                    )

                if (
                    mindepth is not None
                    and maxdepth is not None
                    and not compare(float(mindepth), float(maxdepth))
                ):
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.MAX_MIN_DEPTH,
                        code=400,
                        version=PATCH_VERSION,
                    )

                if (
                    minmagnitude is not None
                    and maxmagnitude is not None
                    and not compare(float(minmagnitude), float(maxmagnitude))
                ):
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.MAX_MIN_MAGNITUDE,
                        code=400,
                        version=PATCH_VERSION,
                    )

                if not Args.isValidCollector(collector):
                    return error_request(
                        msg=Error._400_,
                        msg_detail=f"{Error.UNKNOWN_COLLECTOR} {collector}",
                        code=400,
                        version=PATCH_VERSION,
                    )

                result = getEventsArchivByFilter(
                    before,
                    after,
                    minlatitude,
                    maxlatitude,
                    collector,
                    minlongitude,
                    maxlongitude,
                    mindepth,
                    maxdepth,
                    minmagnitude,
                    maxmagnitude,
                    agency,
                    format,
                )
                return result
        except Exception as e:
            logger_Assembleddata.exception(str(e))
            return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


class Docs_assembleddata(Resource):
    def get(self):
        """
        Get  the user documentation  page
        """
        current_app.api.representations["*/*"] = general
        return render_template("assembleddataDoc.html")


class Version(Resource):
    def get(self):
        """
        Get the version of WebService
        """
        try:

            current_app.api.representations["*/*"] = general
            return PATCH_VERSION

        except Exception as e:
            logger_Assembleddata.exception(str(e))
            return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


class Application(Resource):
    def get(self):

        """
        Get  the  Webservice  application.wadl
        """
        try:

            CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
            current_app.api.representations = {"application/xml": output_xml}
            x = open(CURRENT_DIR + "/application.xml", "r")
            wadlXML = x.read()

            return wadlXML
        except Exception as e:
            logger_Assembleddata.exception(str(e))
            return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)
