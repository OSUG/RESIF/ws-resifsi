"""
This module contain all functions to process querys.
"""
from os import listdir
import io
from datetime import datetime

from flask import send_file, request, Response, make_response
import zipfile
import zlib
import gzip
from flask import current_app
from flask_restplus.representations import output_json
from dateutil import parser

from .__version__ import PATCH_VERSION
from .models import ResifAssembleddata
from .utils import CheckArgAssembleddata as Args, EVENTS_DIR
from .ZipBuffer import ZipBuffer
from ..extensions.UtilErrors import error_request, Error
from ..extensions.representations import general


import decimal
import os


from apps import logger_Assembleddata


def compare(min, max):
    """
    Compare if really the given Min value is below to given Max value

    :param  min : min value given in URL
    :param  max : max value given in URL

    """
    if min > max:
        return False
    else:
        return True


def error_time_handler(before, after):
    """
    chech  time :parameters

    :param datetime before : before datatime  given in URL
    :param  datetime after : after datetime given in URL

    """

    if before is None or after is None:
        return error_request(
            msg=Error._400_,
            msg_detail=Error.REQUIRED_TIME,
            code=400,
            version=PATCH_VERSION,
        )

    if not Args.isValidTime(before):
        return error_request(
            msg=Error._400_,
            msg_detail=Error.INVL_TIME + before,
            code=400,
            version=PATCH_VERSION,
        )

    if not Args.isValidTime(after):
        return error_request(
            msg=Error._400_,
            msg_detail=Error.INVL_TIME + after,
            code=400,
            version=PATCH_VERSION,
        )

    if not compare(after.replace("T", " "), before.replace("T", " ")):
        return error_request(
            msg=Error._400_,
            msg_detail=Error.BEFORE_AFTER,
            code=400,
            version=PATCH_VERSION,
        )
    logger_Assembleddata.debug(
        f"{ type(parser.parse(before))} - {parser.parse(after)} {(parser.parse(before)-parser.parse(after)).days }"
    )
    if (parser.parse(before) - parser.parse(after)).days > 31:
        return error_request(
            msg=Error._203_, msg_detail=Error.MAX, code=203, version=PATCH_VERSION
        )
    return None


def generate_zipped_stream(listtuple):

    """
     Generate  Zip file

    :param list listtuple : list of paths to arcive in disk

    """
    sink = ZipBuffer()
    # archive = zipfile.ZipFile(sink, "w")
    with zipfile.ZipFile(sink, "w", zipfile.ZIP_STORED, True) as archiv:
        for tuple_f in listtuple:
            listfiles = [f"{EVENTS_DIR}/{path}" for path in list(tuple_f)]
            for file_name in listfiles:
                name = file_name.split("/")[-1]
                archiv.write(
                    f"{file_name}", arcname=f"{name}", compress_type=zipfile.ZIP_STORED
                )

                for chunk in sink.get_and_clear():
                    yield chunk

    # close() generates some more data, so we yield that too
    for chunk in sink.get_and_clear():
        yield chunk


def getEventsArchivByName(name, format):

    """
     Process query to get zip with his name. The inventary can be
    returned with TEXT and JSON format.

    :param str name: name of archive
    :param str format: format of response

    """

    try:

        if not Args.isValidFormat(format):
            return error_request(
                msg_detail=Error.INVALID_FORMAT,
                msg=Error._400_,
                code=400,
                version=PATCH_VERSION,
            )

        result = current_app.db_Assembleddata.getArchiveByName(name, format)
        logger_Assembleddata.debug(f"results: {result}")
        if type(result) is dict:
            return error_request(
                msg=result["msg"], code=result["code"], version=PATCH_VERSION
            )

        if format == "archive":

            listfiles = [path for path in result]
            logger_Assembleddata.debug(f"before get {listfiles}")
            response = Response(
                generate_zipped_stream(listfiles), mimetype="application/zip"
            )
            response.headers[
                "Content-Disposition"
            ] = f"attachment; filename=Event_{name}.zip"
            return response
        elif format == "json":
            headers = [
                "collector",
                "name",
                "latitude",
                "longitude",
                "depth",
                "magnitude",
                "magtype",
                "description",
                "agency",
                "pga",
            ]
            jsonResult = getJsonFilename(headers, result)
            current_app.api.representations = {"application/Json": output_json}
            return jsonResult
        else:

            filename = generateTextFilenames(result)
            current_app.api.representations = {"*/*": general}
            return filename

    except Exception as e:
        logger_Assembleddata.exception(str(e))
        return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


def generateTextFilenames(listtuples):
    """
    Convert list tuple to  str

    :param list listtuples: The event inventory

    """
    headers = "#collector|name|latitude|longitude|depth|magnitude|magtype|description|agency|pga"
    text = ""
    for tuple_f in listtuples:
        toStr = [str(elmnt) for elmnt in list(tuple_f)]
        text += "|".join(toStr)
        text += "\n"

    return headers + "\n" + text


def getJsonFilename(headers, tuples):
    """
    Convert  tuples to Json

    :param  list  headers: The list of Json element's keys
    :param tuple tuples: l'inventair en format Json

    """
    jsonResult = []
    for tuple_row in tuples:
        row_json = {}
        for x, y in zip(headers, list(tuple_row)):
            if type(y) is decimal.Decimal:
                row_json[x] = str(y)
            else:
                row_json[x] = y

        jsonResult.append(row_json)
    return jsonResult


def getEventsArchivByFilter(
    before,
    after,
    minlatitude,
    maxlatitude,
    collector,
    minlongitude,
    maxlongitude,
    mindepth,
    maxdepth,
    minmagnitude,
    maxmagnitude,
    agency,
    format,
):
    """
        Proccess query to get events with filter.

    :param  datatime before: datatime filter
    :param datatime after: datatime filter
    :param minlatitude: The minimum  value of latitude
    :param maxlatitude: The maximum  value of latitude
    :param collector: The collector of events
    :param minlongitude: The minimum value of longitude
    :param maxlongitude:The maximum  value of longitude
    :param mindepth: The minimum  value of depth
    :param maxlatitude: The maximum  value of depth
    :param maxdepth: The maximum  vaue of depth
    :param minmagnitude:  The minimum  value of magnitude
    :param maxmagnitude: The maximum  value of magnitude
    :param agency:
    :param format: The format of results : archive (zip) and TEXT or JSON of inventory


    """
    if not Args.isValidFormat(format):
        return error_request(
            msg_detail=Error.INVALID_FORMAT,
            msg=Error._400_,
            code=400,
            version=PATCH_VERSION,
        )

    result = current_app.db_Assembleddata.getPathArchivByFilter(
        before,
        after,
        minlatitude,
        maxlatitude,
        collector,
        minlongitude,
        maxlongitude,
        mindepth,
        maxdepth,
        minmagnitude,
        maxmagnitude,
        agency,
        format,
    )
    if type(result) is dict:
        return error_request(
            msg_detail=result["msg"],
            msg=Error._201_,
            code=result["code"],
            version=PATCH_VERSION,
        )
    logger_Assembleddata.debug("results: {result}")

    if format == "archive":

        listfiles = [path for path in list(result)]

        logger_Assembleddata.debug(listfiles)
        response = Response(
            generate_zipped_stream(listfiles), mimetype="application/zip"
        )
        response.headers[
            "Content-Disposition"
        ] = f"attachment; filename=Events_{before}_{after}.zip"
        return response
    elif format == "json":
        headers = [
            "collector",
            "name",
            "latitude",
            "longitude",
            "depth",
            "magnitude",
            "magtype",
            "description",
            "agency",
            "pga",
        ]
        jsonResult = getJsonFilename(headers, result)
        current_app.api.representations = {"application/json": output_json}

        return jsonResult

    else:

        logger_Assembleddata.debug("results: {result}")
        if type(result) is dict:
            return error_request(
                msg=result["msg"], code=result["code"], version=PATCH_VERSION
            )
        filename = generateTextFilenames(result)
        current_app.api.representations = {"*/*": general}
        return filename
