class ZipBuffer(object):
    """A file-like object for zipfile.ZipFile to write into.

    Attributes:
        data (list): stocker les données
        pos (int): position
    """

    def __init__(self):
        self.data = []
        self.pos = 0

    def write(self, data):
        """
        Writ edata in Buffer
        :param object data: data readed from file
        """
        self.data.append(data)
        self.pos += len(data)

    def tell(self):
        """
        Return position

        """
        return self.pos

    def flush(self):
        """
         This function is used by zipfile to write in buffer.
        Remove it will be raise exception.
        """
        pass

    def get_and_clear(self):
        """
        Get data and clear the buffer
        """

        result = self.data
        self.data = []
        return result
