import os
import psycopg2
from apps.extensions.manage import DatabaseConnection
from datetime import datetime
from .parameters import *
from flask import current_app
from .utils import *
from apps import logger_Assembleddata
from ..extensions.UtilErrors import Error


class ResifAssembleddata(DatabaseConnection):
    def __init__(self):

        result = DatabaseConnection.__init__(self, database, user, password, host)
        if type(result) is str:
            logger_Assembleddata.info(" Failed connection to database " + result)
            return None
        else:
            logger_Assembleddata.info(" Success connection to database " + database)
            self._conn = result["conn"]
            self._cur = result["cur"]

    def executeQuery(self, query, selectquery=False):

        """
        Execute Query

        :param str query: contain query to excute
        :param bool selectquery: If the value is True the result of query are fetched.

        """

        try:
            if self._cur.closed or self._conn.closed:
                DatabaseConnection.reconnect(self, database, user, password, host)
                logger_Assembleddata.info(
                    " Success Reconnection to database " + database
                )

            logger_Assembleddata.info(
                " connection vars " + str(self._cur.closed) + str(self._conn.closed)
            )
            result = self._cur.execute(query)
            result = self._conn.commit()

            if selectquery:
                result = self._cur.fetchall()

            return result

        except Exception as e:
            self._conn.rollback()
            logger_Assembleddata.exception(str(e))

            return {"msg": "  ", "code": 500}

    def getArchiveByName(self, name, format):
        """
         Return inventary or path to archive events

        :param str name : The name of event to get
        :param str format: type of result wanted :  TEXT/JSON for inventary or ARCHIVE for zip

        """
        if format in ["text", "json", None]:
            query = f"SELECT  collector, name, latitude, longitude, depth,\
					  magnitude, magtype, description, agency, pga\
					  FROM assembleddata\
					  WHERE name = '{name}'"
        else:
            query = f"SELECT  text_file, archive_file\
					FROM assembleddata\
					WHERE name = '{name}' "

        logger_Assembleddata.debug(f" query : {query}")
        result = self.executeQuery(query, True)
        logger_Assembleddata.debug(f"result of query : {query}")
        if result == []:
            return {"msg": Error._201_, "code": 201}
        return result

    def getPathArchivByFilter(
        self,
        before,
        after,
        minlatitude,
        maxlatitude,
        collector,
        minlongitude,
        maxlongitude,
        mindepth,
        maxdepth,
        minmagnitude,
        maxmagnitude,
        agency,
        format,
    ):
        """
         Apply filters to obtain specific events

        :parm str args: values to filter result to get events

        """

        if format in ["text", "json", None]:

            query = f"SELECT  collector, name, latitude, longitude, depth,\
					  magnitude, magtype, description, agency, pga\
					  FROM assembleddata "
        else:
            query = f"SELECT  text_file, archive_file FROM assembleddata "

        where = f" WHERE "
        whereClauses = []
        if after is not None:
            whereClauses.append(f" event_date >= '{after}'")

        if before is not None:
            whereClauses.append(f" event_date <= '{before}' ")

        if minlatitude is not None:
            whereClauses.append(f" latitude >= {float(minlatitude)} ")

        if maxlatitude is not None:
            whereClauses.append(f" latitude <= {float(maxlatitude)} ")

        if collector is not None:
            whereClauses.append(f" collector = '{collector}'")

        if minlongitude is not None:
            whereClauses.append(f" longitude >= {float(minlongitude)} ")

        if maxlongitude is not None:
            whereClauses.append(f" longitude <= {float(maxlongitude)} ")

        if minmagnitude is not None:
            whereClauses.append(f" magnitude >= {float(minmagnitude)} ")

        if maxmagnitude is not None:
            whereClauses.append(f" magnitude <= {float(maxmagnitude)} ")

        if mindepth is not None:
            whereClauses.append(f" depth >= {float(mindepth)} ")

        if maxdepth is not None:
            whereClauses.append(f" depth <= {float(maxdepth)} ")

        if agency is not None:
            whereClauses.append(f" agency = '{agency}' ")

        where += " and ".join(whereClauses)
        query += where
        query += "order by name asc"
        logger_Assembleddata.debug(f" Query : {query} ")
        result = self.executeQuery(query, True)
        logger_Assembleddata.debug(len(result))

        if result == []:
            return {"msg": Error._201_, "code": 201}
        return result

    def close_connection(self):
        """ Close the connection """

        try:
            self._cur.close()
            self._conn.close()
            logger_Assembleddata.debug(" Connection Database Closed")

        except Exception as e:
            logger_Assembleddata.error(str(e))
