"""
 Get logger defined in logging*.ini
"""
from flask_restplus import Api, Resource
from flask import Response, make_response

import logging
import os


logger_Transaction = logging.getLogger("Transaction")
logger_Orphanfile = logging.getLogger("Orphanfile")
logger_Assembleddata = logging.getLogger("Assembleddata")
