from flask_restplus import Resource, reqparse
from flask import request, current_app, render_template
from .lib import (
    get_data_file_level,
    get_data_transaction_level,
    get_content_transaction,
)
from .utils import CheckArgTransaction as Args, MAX_ARGUMENTS, URL_ARGUMENTS
from apps import logger_Transaction
from .__version__ import PATCH_VERSION
from ..extensions.UtilErrors import error_request, Error
from ..extensions.representations import general


class Transaction(Resource):
    def get(self):
        """
        - Retrieve the parameters of the requests
        - The management of parameter errors
        - Calling up the corresponding functions

        """
        try:
            # check error in arguments
            logger_Transaction.info("Parse arguments for URL" + str(request.url))
            args_request = request.args.copy()
            list_args_request = list(args_request.items(multi=True))  # avec doublant

            if len(list_args_request) > MAX_ARGUMENTS:
                return error_request(
                    msg=Error._400_,
                    msg_detail=Error.LEN_ARGS,
                    code=400,
                    version=PATCH_VERSION,
                )

            for arg in args_request.keys():
                if arg not in URL_ARGUMENTS:
                    return error_request(
                        msg=Error._400_,
                        msg_detail=Error.UNKNOWN_PARM + arg,
                        code=400,
                        version=PATCH_VERSION,
                    )

            # parse argument
            parser = reqparse.RequestParser()
            parser.add_argument("starttime", type=str)
            parser.add_argument("endtime", type=str)
            parser.add_argument("node", type=str)
            parser.add_argument("network", type=str)
            parser.add_argument("type", type=str)
            parser.add_argument("format", type=str)
            parser.add_argument("status", type=str)
            parser.add_argument("transactionid", type=str)
            parser.add_argument("erroron", type=str)
            parser.add_argument("level", type=str, default="transaction")

            args = parser.parse_args()

            start = args.get("starttime", None)
            end = args.get("endtime", None)
            node = args.get("node", None)
            network = args.get("network", None)
            format = args.get("format", None)
            type_tr = args.get("type", None)
            level = args.get("level", None)
            transactionid = args.get("transactionid", None)
            status = args.get("status", None)
            erroron = args.get("erroron", None)

            if not Args.isLevelValide(level):
                return error_request(
                    msg=Error._400_,
                    msg_detail=Error.INVL_LEVEL,
                    code=400,
                    version=PATCH_VERSION,
                )

            if transactionid != None and not Args.isTransactionIdValide(transactionid):
                return error_request(
                    msg=Error._400_,
                    msg_detail=Error.INVL_TRANS_ID,
                    code=400,
                    version=PATCH_VERSION,
                )

            if format != None and not Args.isValidFormat(format):
                return error_request(
                    msg=Error._400_,
                    msg_detail=f"{Error.INVL_FORMAT} {format}",
                    code=400,
                    version=PATCH_VERSION,
                )

            if level == "content":
                return get_content_transaction(transactionid)

            elif level == "file":

                return get_data_file_level(
                    start,
                    end,
                    node,
                    network,
                    format,
                    type_tr,
                    level,
                    transactionid,
                    status,
                    erroron,
                )
            else:
                if (
                    start is None
                    and end is None
                    and node is None
                    and network is None
                    and type_tr is None
                    and transactionid is None
                    and status is None
                    and erroron is None
                ):
                    return get_data_transaction_level(
                        start,
                        end,
                        node,
                        network,
                        format,
                        type_tr,
                        level,
                        transactionid,
                        status,
                        erroron,
                        limit=True,
                    )
                else:
                    return get_data_transaction_level(
                        start,
                        end,
                        node,
                        network,
                        format,
                        type_tr,
                        level,
                        transactionid,
                        status,
                        erroron,
                    )
        except Exception as e:
            logger_Transaction.exception(str(e))
            return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


class Version(Resource):
    def get(self):
        """
        Get the version of WebService

        """
        try:

            current_app.api.representations["*/*"] = general
            return PATCH_VERSION

        except Exception as e:
            logger_Transaction.exception(str(e))
            return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


class Application(Resource):
    def get(self):

        """
        Get  the  Webservice  application.wadl
        """
        try:

            CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
            current_app.api.representations = {"application/xml": general}
            x = open(CURRENT_DIR + "/application.xml", "r")
            wadlXML = x.read()

            return wadlXML

        except Exception as e:
            logger_Transaction.exception(str(e))
            return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


class Docs_transaction(Resource):
    """
    Get  the user documentation  page
    """

    def get(self):
        print("here")
        current_app.api.representations["*/*"] = general
        return render_template("transactionDoc.html")
