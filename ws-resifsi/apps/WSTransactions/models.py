import os
import psycopg2
import re

from datetime import datetime

from .parameters import *
from apps.extensions.manage import DatabaseConnection
from apps import logger_Transaction
from .utils import *
from ..extensions.UtilErrors import error_request, Error
from .utils import CheckArgTransaction as Args


class ResifTransactionIntgaration(DatabaseConnection):
    def __init__(self):

        result = DatabaseConnection.__init__(self, database, user, password, host)
        if type(result) is str:
            logger_Transaction.info(" Failed connection to database " + result)
            return None
        else:
            logger_Transaction.info(
                " Success connection to database "
                + database
                + " "
                + str(self._conn)
                + " , "
                + str(self._cur)
            )
            self._conn = result["conn"]
            self._cur = result["cur"]

    def executeQuery(self, query, selectquery=False):

        """
        Execute Query

        :param str query: contain query to excute
        :param bool selectquery: If the value is True the result of query are fetched.

        """
        try:
            if self._cur.closed or self._conn.closed:
                DatabaseConnection.reconnect(self, database, user, password, host)
                logger_Transaction.info(" Success Reconnection to database " + database)
            logger_Transaction.info(
                " connection vars " + str(self._cur.closed) + str(self._conn.closed)
            )
            result = self._cur.execute("set statement_timeout to " + TIMEOUT)
            result = self._conn.commit()

            result = self._cur.execute(query)
            result = self._conn.commit()

            if selectquery:
                result = self._cur.fetchall()

            return result

        except Exception as e:

            self._conn.rollback()
            logger_Transaction.exception(str(e))
            if "canceling statement due to statement timeout" in str(e):
                return {"msg": Error._203_, "code": 203}

            return {"msg": "  ", "code": 500}

    def get_transaction(self, transaction_name):
        """"""
        try:

            query = (
                "SELECT transaction_id FROM TRANSACTION\
                     WHERE transaction_name = '"
                + transaction_name
                + "'"
            )
            result = self.executeQuery(query, True)
            return result

        except Exception as e:
            logger_Transaction.error(str(e))
            return {"msg": Error.PROCESSING, "code": 500}

    def get_transactions(
        self, start, end, node, code, type_tr, status, erroron, limit, transactionid
    ):

        try:

            mesg_error = " "
            query = "SELECT transaction_name, resif_node "

            from_query = " FROM  transaction_process P "
            where_query = (
                " INNER JOIN  transaction t ON ( P.transaction_id = t.transaction_id  "
            )
            groupby = " GROUP BY transaction_name, resif_node,  lastupdate,\
                        total_file, transaction_status, transaction_type"

            orderby = " ORDER BY lastupdate Desc"

            if transactionid is not None:
                where_query += " and t.transaction_name ='" + transactionid + "'"
            if code is None:
                # query+=" , array_to_string ( getnetworks(file_name), ',') as network"
                query += ", array_to_string (array_agg(distinct split_part(file_name,'.',1)), ',') as network"
            else:
                if not Args.isValidNetwork(code):
                    return {"msg": Error.INVL_NETWORK + code, "code": 400}

                query += ", '" + code + "' as network "
                regulr_expre = "^(" + code + "[.])"
                where_query += " and file_name ~ '" + regulr_expre + "'"

            query += ",lastupdate, count(*) as total_file, transaction_status,  transaction_type"

            if start is not None:
                where_query += " and lastupdate  >=  '" + start + "'"

            if end is not None:
                where_query += " and  lastupdate  <=  '" + end + "'"

            if node is not None:
                if not Args.isValidateNode(node):
                    return {"msg": Error.INVL_NODE + node, "code": 400}

                where_query += " and  resif_node = '" + node + "'"

            if type_tr is not None:
                if not Args.isTypeValide(type_tr):
                    return {"msg": Error.INVL_TYPE + type_tr, "code": 400}
                where_query += " and  transaction_type = '" + type_tr + "'"

            if status is not None:
                if not Args.isStatusValide(status):
                    return {"msg": Error.INVL_STATUS + status, "code": 400}

                where_query += " and  transaction_status = " + status + ""

            if erroron is not None:
                if not Args.isTestValide(erroron):
                    return {"msg": Error.INVL_TEST + erroron, "code": 400}

                where_query += " and " + erroron + " = false "

            query += from_query + where_query + " )" + groupby + orderby

            result = self.executeQuery(query, True)
            return result

        except Exception as e:
            logger_Transaction.error(str(e))
            return {"msg": Error.PROCESSING, "code": 500}  # error in processing

    def get_files(self, transaction_name, erroron=None):

        try:

            if self.get_transaction(transaction_name) == []:
                return {"msg": Error.TRANS_NOEXIST, "code": 201}

            query = " SELECT transaction_name, resif_node, lastupdate, file_name, first_error"
            from_query = " FROM  transaction T"
            join_query = (
                " INNER JOIN  transaction_process P ON ( T.transaction_id = P.transaction_id and transaction_name='"
                + transaction_name
                + "'"
            )
            if erroron is not None:
                if not Args.isTestValide(erroron):
                    return {Error.INVL_TEST + erroron}
                join_query += " and " + erroron + " = false "

            query += from_query + join_query + " )"

            result = self.executeQuery(query, True)

            return result
        except Exception as e:
            logger_Transaction.error(str(e))
            return {"msg": Error.PROCESSING, "code": 500}

    def get_last_transactions(self):

        try:

            query = " SELECT transaction_name, resif_node ,\
                    array_to_string (array_agg(distinct substring( file_name from 1 for 2)), ',') as network,\
                    lastupdate, count(*) as total_file, transaction_status, transaction_type\
                    FROM  transaction_process P,\
                        (select transaction_id  , transaction_name,\
                        resif_node , lastupdate,total_file,\
                        transaction_status, transaction_type\
                        from  transaction\
                        order by lastupdate Desc limit 100) as last_trans\
                    where last_trans.transaction_id = P.transaction_id\
                    GROUP BY transaction_name, resif_node,  lastupdate, total_file, transaction_status, transaction_type"
            result = self.executeQuery(query, True)
            return result

        except Exception as e:
            logger_Transaction.exception(str(e))
            return {"msg": Error.PROCESSING, "code": 500}
