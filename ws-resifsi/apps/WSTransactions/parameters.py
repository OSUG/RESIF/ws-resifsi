"""
The file contains the parameters for connecting to the database.

A parameters.py file in WS* must contain 
the connection parameters

"""
import os

host = os.getenv('TRANSACTION_PGHOST', 'localhost')
database = os.getenv('TRANSACTION_PGDATABASE', 'resifintegration')
user = os.getenv('TRANSACTION_PGUSER', 'wsresifsi')
password = os.getenv('TRANSACTION_PGPASSWORD', '')
