import re
import os
from ..extensions.UtilValidate import CheckArg

MAX_ARGUMENTS = 10
URL_ARGUMENTS = [
    "level",
    "starttime",
    "endtime",
    "network",
    "node",
    "type",
    "format",
    "status",
    "transactionid",
    "erroron",
]
LEVEL_LIST = ["content", "transaction", "file"]
NODES_LIST = ["OMIV", "IPGP", "RAP", "SISMOB", "DASE", "RLBP-OCA", "RLBP"]
TYPETRANSACTION_LIST = ["seismic_data_miniseed", "seismic_metadata_dataless_seed"]
LIMIT_ROWS = 100
STATUS_VALUE = ["0", "4", "8", "128"]
TRANSACTIONS_DIR = os.getenv('TRANSACTIONS_DIR', '/data')
TIMEOUT = "10000"
FORMAT_LIST = ["text", "json", None]


class CheckArgTransaction(CheckArg):
    def isLevelValide(level):
        return level in LEVEL_LIST

    def isValidDatatype(datatype):
        return datatype in DATATYPE_LIST

    # source pierre (a vérifier)
    def isValidNetwork(network):
        return re.match("^[A-Za-z0-9\*\?]{1,2}$", network) if network else False

    def isValidateNode(node):
        return node in NODES_LIST

    def isStatusValide(status):
        return status in STATUS_VALUE

    def isTestValide(test):

        return re.match("^T([0-9]$|[0-1][0-5]$)", test) if test else False

    def isTypeValide(type_tr):
        return type_tr in TYPETRANSACTION_LIST

    def isTransactionIdValide(transaction_name):
        return (
            re.match("^[A-Z0-9]{8}$", transaction_name) if transaction_name else False
        )

    def isValidFormat(format):

        return format in FORMAT_LIST


"""
class Error(object) :

	_400_ = "Some of your query parameters are invalid or non-coherent"
	_500_ = "Internal Server Error" 
	_403_ = " You are requesting too much data in a single request. Please split your request in smaller ones"
	_201_ = "No data"

	LEN_ARGS = " Too much arguments in URL."
	UNKNOWN_PARM = ' Unknown query parameter '
	INVL_LEVEL = " Invalid level arguments value." 
	INVL_NODE = " Invalid node "
	INVL_NETWORK = " Invalid network code "
	INVL_TIME = " Bad date value "
	INVL_TYPE = " Invalid type "
	INVL_STATUS = " Invalid transaction  status "
	INVL_TEST = " Invalid test code "
	INVL_TRANS = " Invalid transaction type "
	START_LATER = " Starttime cannot be later than endtime : "
	TRANS_NOEXIST = " Transaction doesn't exist "
	TRANS_REQUIRED = " Transaction name is mandatory " 
	PROCESSING = " Error processing your request "
	BAD_ARGS = " You use arguments for transaction level "

"""
