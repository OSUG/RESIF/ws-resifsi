"""
This module contain all functions to process querys.

"""

import uuid
import os
import collections
import urllib.request
from datetime import datetime

from flask import make_response, send_file, request, current_app
from simplexml import dumps
from flask_restplus import abort
from time import strftime
from flask_restplus.representations import output_json

from .models import ResifTransactionIntgaration
from apps import logger_Transaction
from .utils import *
from .__version__ import PATCH_VERSION
from .utils import CheckArgTransaction as Args
from ..extensions.UtilErrors import error_request, Error, error_handler
from ..extensions.representations import general


def jsonTotext(json_result, header, limit=False):
    """
    Convert Json to text

    :param json_result: json object
    :param header: str contain the colums name

    """
    if limit:
        result_text = "# those are the last 100 transactions\n" + header
        json_result.pop(0)
    else:
        result_text = header

    for entry in json_result:
        listEntry = ["%s" % str(c) for c, i in zip(entry.values(), entry.keys())]
        row = "|".join(listEntry)
        result_text += "\n " + row
    return result_text


def tupleTojson(keys, list_tuple, limit=False):
    """
     Convert tuple to json.

    :param list keys: list of columns name
    :param tuple liste_tuple: tuple of query result
    :param bool limit:  if True add comment " ..". No filter sended.

    """

    json_result = []

    if limit:
        dictOrd = collections.OrderedDict()
        dictOrd["_comment"] = "# those are the last 100 transactions "
        json_result.append(dictOrd)

    for tuple_i in list_tuple:
        dictOrd = collections.OrderedDict()
        for j, i in zip(tuple_i, keys):
            if isinstance(j, datetime):
                dictOrd[i] = str(j)
            else:
                dictOrd[i] = j
        if "type" in dictOrd.keys():
            if dictOrd["type"] in (
                "seismic_metadata_dataless_seed",
                "seismic_data_ph5",
            ):
                dictOrd["network"] = "N/A"
        json_result.append(dictOrd)

    return json_result


def response_code(result):
    """
    Handle the return  code of query
    """

    if len(result) == 0:
        return error_request(msg=Error._201_, code=201, version=PATCH_VERSION)
    else:
        return 200


def get_data_transaction_level(
    start,
    end,
    node,
    network,
    format,
    type_tr,
    level,
    transactionid,
    status,
    erroron,
    limit=False,
):

    """
    Return Transactions in format json or text according to Format filter.
    Others filters is applied if they are not None.
    """

    try:
        logger_Transaction.debug(" In get transaction level")

        if start is not None and not Args.isValidTime(start):
            return error_request(
                msg=Error._400_,
                msg_detail=Error.INVL_TIME + start,
                code=400,
                version=PATCH_VERSION,
            )
        if end is not None and not Args.isValidTime(end):
            return error_request(
                msg=Error._400_,
                msg_detail=Error.INVL_TIME + end,
                code=400,
                version=PATCH_VERSION,
            )
        if start is not None and end is not None and start > end:
            start = start.replace("T", " ")
            end = end.replace("T", " ")
            return error_request(
                msg=Error._400_,
                msg_detail=Error.START_LATER + start + " > " + end,
                code=400,
                version=PATCH_VERSION,
            )

        else:

            if not limit:
                result = current_app.db_Transaction.get_transactions(
                    start,
                    end,
                    node,
                    network,
                    type_tr,
                    status,
                    erroron,
                    limit,
                    transactionid,
                )
            else:
                result = current_app.db_Transaction.get_last_transactions()

            if type(result) is dict:
                if result["code"] == 400:
                    return error_request(
                        msg=Error._400_,
                        msg_detail=result["msg"],
                        code=result["code"],
                        version=PATCH_VERSION,
                    )
                elif result["code"] == 500:
                    return error_request(
                        msg=Error._500_,
                        msg_detail=result["msg"],
                        code=result["code"],
                        version=PATCH_VERSION,
                    )
                else:
                    return error_request(
                        msg=result["msg"], code=result["code"], version=PATCH_VERSION
                    )

            keys = [
                "transactionID",
                "node",
                "network",
                "last-update",
                "number of files",
                "status",
                "type",
            ]

            result_json = tupleTojson(keys, result, limit)

            # Analyse len of results
            code = response_code(result_json)
            if code != 200:
                return code

            if format == "text":

                header = " # transactionID|node|networks(in files names)|last-update|number of files|status|type"
                result_text = jsonTotext(result_json, header, limit)
                current_app.api.representations = {"*/*": general}
                return result_text

            else:
                current_app.api.representations = {"application/json": output_json}
                return result_json

    except Exception as e:

        logger_Transaction.exception(str(e))
        return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


def get_data_file_level(
    start, end, node, network, format, type_tr, level, transactionid, status, erroron
):

    """
    Handle file level.  return the files informations of the transaction. The transcationid is mandotary.
    """

    try:

        if transactionid is None:
            return error_request(
                msg=Error._400_,
                msg_detail=Error.TRANS_REQUIRED,
                code=400,
                version=PATCH_VERSION,
            )

        if (
            start is not None
            or end is not None
            or node is not None
            or network is not None
            or type_tr is not None
            or status is not None
        ):
            return error_request(
                msg=Error._400_,
                msg_detail=Error.BAD_ARGS,
                code=400,
                version=PATCH_VERSION,
            )

        result = current_app.db_Transaction.get_files(transactionid, erroron)

        if type(result) is dict:
            if result["code"] == 400:
                return error_request(
                    msg=Error._400_,
                    msg_detail=result["msg"],
                    code=result["code"],
                    version=PATCH_VERSION,
                )
            else:
                return error_request(
                    msg=Error._500_,
                    msg_detail=result["msg"],
                    code=result["code"],
                    version=PATCH_VERSION,
                )

        keys = [
            "transactionID",
            "node",
            "last-update",
            "filename",
            "first error on testID",
        ]
        result_json = tupleTojson(keys, result)

        # Analyse len of results

        code = response_code(result_json)
        if code != 200:
            return code
        if format == "text":
            header = "# transactionID|node|last-update|filename| first error on testID"
            result_text = jsonTotext(result_json, header)
            current_app.api.representations = {"*/*": general}
            return result_text

        else:
            current_app.api.representations = {"application/json": output_json}
            return result_json

    except Exception as e:
        logger_Transaction.exception(str(e))
        return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


def get_content_transaction(transactionid):
    """
    Return the xml file of the tranaction because FORMAT value is CONTENT.
    """

    try:

        if transactionid is None:
            return error_request(
                msg=Error._400_,
                msg_detail=Error.TRANS_REQUIRED,
                code=400,
                version=PATCH_VERSION,
            )
        result = current_app.db_Transaction.get_transaction(transactionid)
        if result == []:
            return error_request(
                msg=Error._201_,
                msg_detail=Error.TRANS_NOEXIST,
                code=201,
                version=PATCH_VERSION,
            )
        elif type(result) is dict:
            return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)

        current_app.api.representations = {"application/xml": general}
        try:
            x = open(TRANSACTIONS_DIR+ "/" + transactionid + ".xml", 'r')
            transactionXML = x.read()
        except:
            # Mince je fais quoi ici ?
            transactionXML = "<Error>No data</Error>"

        return transactionXML

    except Exception as e:
        logger_Transaction.exception(str(e))
        return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)
