"""
 Create Assembleddata  namespace with  its resources.

"""

import os
from flask_restplus import Resource, Namespace, reqparse
from ..extensions.UtilErrors import error_request

from .resources import *
from .__version__ import MAJOR_VERSION

transaction_ns = Namespace("transaction", description="Transaction description")

transaction_ns.add_resource(Transaction, "/" + MAJOR_VERSION + "/query")
transaction_ns.add_resource(Version, f"/{MAJOR_VERSION}/version")
transaction_ns.add_resource(Application, f"/{MAJOR_VERSION}/application.wadl")
transaction_ns.add_resource(Docs_transaction, f"/{MAJOR_VERSION}/", "/")
