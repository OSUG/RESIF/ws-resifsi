import os
import psycopg2
import re

from datetime import datetime

from .parameters import *
from apps.extensions.manage import DatabaseConnection
from apps import logger_Orphanfile

from .utils import CheckArgOrphanfile as Args, TIMEOUT
from apps.extensions.UtilErrors import Error


class ResifOrphanfile(DatabaseConnection):
    def __init__(self):

        result = DatabaseConnection.__init__(self, database, user, password, host)
        if type(result) is str:
            logger_Orphanfile.info(" Failed connection to database " + result)
            return None
        else:
            logger_Orphanfile.info(" Success connection to database " + database)
            self._conn = result["conn"]
            self._cur = result["cur"]

    def executeQuery(self, query, selectquery=False):

        """
        Execute Query

        :param str query: contain query to excute
        :param bool selectquery: If the value is True the result of query are fetched.

        """

        try:

            if self._cur.closed or self._conn.closed:

                DatabaseConnection.reconnect(self, database, user, password, host)
                logger_Orphanfile.info(" Success Reconnection to database " + database)
            logger_Orphanfile.info(
                " connection vars " + str(self._cur.closed) + str(self._conn.closed)
            )
            result = self._cur.execute(query)
            result = self._conn.commit()
            if selectquery:
                result = self._cur.fetchall()
            return result

        except Exception as e:
            self._conn.rollback()
            logger_Orphanfile.exception(str(e))
            if "canceling statement due to statement timeout" in str(e):
                return {"msg": Error._203_, "code": 203}

            return {"msg": str(e), "code": 500}

    def getOrphanfile(
        self, tablename, networks, stations, channels, starttime, endtime, location
    ):

        try:

            select = "SELECT source_file"
            fromquery = " FROM " + tablename
            where = (
                "  WHERE  channel_id=0 and network like ANY ('{"
                + networks.replace("?", "_")
                + "}')"
            )
            ordeby = "ORDER BY source_file"

            if stations is not None:
                stations_list = stations.replace(" ", "").split(",")
                for station in stations_list:
                    if not Args.isValidStation(station):
                        return {"msg": Error.INVL_STATION + station, "code": 400}
                where += (
                    " and station  like ANY ('{" + stations.replace("?", "_") + "}')"
                )

            if starttime is not None:
                where += " and starttime  >=  '" + starttime + "'"

            if endtime is not None:
                where += " and  endtime  <=  '" + endtime + "'"

            if channels is not None:
                channels_list = channels.replace(" ", "").split(",")
                for channel in channels_list:
                    if not Args.isValidChannel(channel):
                        return {"msg": Error.INVL_CHANNEL + channel, "code": 400}

                where += (
                    " and channel like ANY ('{" + channels.replace("?", "_") + "}')"
                )

            if location is not None:
                location_list = location.replace(" ", "").split(",")
                for location in location_list:
                    if not Args.isValidLocation(location):
                        return {"msg": Error.INVL_LOCATION + location, "code": 400}
                where += (
                    " and location like ANY ('{" + location.replace("?", "_") + "}')"
                )

            query = select + fromquery + where + ordeby
            result = self.executeQuery(query, True)
            return result

        except Exception as e:
            logger_Orphanfile.exception(str(e))
            return {"msg": Error.PROCESSING, "code": 500}  # error in processing
