"""
Define the GET, POST, DELETE functions for each resource.

"""
from flask_restplus import Resource, reqparse
from flask import request, current_app, render_template
import os

from .lib import get_orphanfile
from .utils import *
from apps import logger_Orphanfile
from .__version__ import PATCH_VERSION
from .utils import CheckArgOrphanfile as Args
from ..extensions.UtilErrors import error_request, Error
from ..extensions.representations import general


class Orphanfile(Resource):
    def get(self):

        """Get events
        - Retrieve the parameters of the requests
        - The management of parameter errors
        - Calling up the corresponding functions

        """

        try:

            logger_Orphanfile.info("Parse arguments for URL " + str(request.url))
            args_request = request.args.copy()
            list_args_request = list(args_request.items(multi=True))  # avec doublant

            if Args.isMaxLen(list_args_request, MAX_ARGUMENTS):
                return error_request(
                    msg=Error._400_,
                    msg_detail=Error.LEN_ARGS,
                    code=400,
                    version=PATCH_VERSION,
                )
            isValid, arg = Args.isValid(args_request.keys(), VALID_ARGUMENTS)
            if not isValid:
                return error_request(
                    msg=Error._400_,
                    msg_detail=Error.UNKNOWN_PARM + arg,
                    code=400,
                    version=PATCH_VERSION,
                )

            # parse argument
            parser = reqparse.RequestParser()

            parser.add_argument("network", type=str)
            parser.add_argument("format", type=str)
            parser.add_argument("datatype", type=str, default=VALIDATED)
            parser.add_argument("station", type=str)
            parser.add_argument("channel", type=str)
            parser.add_argument("starttime", type=str)
            parser.add_argument("endtime", type=str)
            parser.add_argument("location", type=str)

            args = parser.parse_args()

            networks = args.get("network", None)
            format = args.get("format", None)
            datatype = args.get("datatype", None)
            station = args.get("station", None)
            channels = args.get("channel", None)
            starttime = args.get("starttime", None)
            endtime = args.get("endtime", None)
            location = args.get("location", None)

            if format != None and not Args.isValidFormat(format):
                return error_request(
                    msg=Error._400_,
                    msg_detail=f"{Error.INVL_FORMAT} {format}",
                    code=400,
                    version=PATCH_VERSION,
                )

            return get_orphanfile(
                networks,
                format,
                datatype,
                station,
                channels,
                starttime,
                endtime,
                location,
            )

        except Exception as e:

            logger_Orphanfile.exception(str(e))
            return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


class Version(Resource):
    def get(self):
        """
        Get the version of WebService
        """
        try:
            current_app.api.representations["*/*"] = general
            return PATCH_VERSION
        except Exception as e:
            logger_Orphanfile.exception(str(e))
            return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


class Application(Resource):
    def get(self):

        """
        Get  the  Webservice  application.wadl
        """
        try:

            CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
            current_app.api.representations = {"application/xml": output_xml}
            x = open(CURRENT_DIR + "/application.xml", "r")
            wadlXML = x.read()

            return wadlXML

        except Exception as e:
            logger_Orphanfile.exception(str(e))
            return error_request(msg=Error._500_, code=500, version=PATCH_VERSION)


class Docs_orphanfile(Resource):
    """
    Get  the user documentation  page
    """

    def get(self):
        current_app.api.representations["*/*"] = general
        return render_template("orphanfileDoc.html")
