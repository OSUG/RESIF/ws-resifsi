"""
This module contain all functions to process querys.
"""

import collections
from .models import ResifOrphanfile
from apps import logger_Orphanfile
from flask import request, make_response, current_app
from datetime import datetime

from .utils import CheckArgOrphanfile as Args, VALIDATED, ALL_RECORDS, BUD_RECORDS
from ..extensions.UtilErrors import Error, error_handler, error_request
from flask_restplus.representations import output_json
from ..extensions.representations import general
from .__version__ import PATCH_VERSION


def response_code(result):
    """
    Handle the return  code of query
    """
    if len(result) == 0:
        return error_request(msg=Error._201_, code=201, version=PATCH_VERSION)
    else:
        return 200


def tupleTojson(keys, list_tuples):
    """
    Convert Tuple to json
    """

    json_result = []

    for tuple_i in list_tuples:
        dictOrd = collections.OrderedDict()
        for j, i in zip(tuple_i, keys):
            if isinstance(j, datetime):
                dictOrd[i] = str(j)
            else:
                dictOrd[i] = j
        json_result.append(dictOrd)

    return json_result


def tupleTotext(keys, list_tuples, header):
    """
    Convert tuple to text
    """

    result_text = header

    for tuple_i in list_tuples:
        listEntry = ["%s" % c for c in tuple_i]
        row = "".join(listEntry)
        result_text += "\n " + row
    return result_text


def get_orphanfile(
    networks, format, datatype, station, channels, starttime, endtime, location
):
    """
    Get list of files don't have a metadata
    """

    try:

        if networks is None:
            return error_request(
                msg=Error._400_,
                msg_detail=Error.NETWORK_REQUIRED,
                code=400,
                version=PATCH_VERSION,
            )

        networks_list = networks.split(",")
        for network in networks_list:
            if not Args.isValidNetwork(network):
                return error_request(
                    msg=Error._400_,
                    msg_detail=Error.INVL_NETWORK + network,
                    code=400,
                    version=PATCH_VERSION,
                )

        if not Args.isValidDatatype(datatype):
            return error_request(
                msg=Error._400_,
                msg_detail=Error.INVL_DATATYPE + datatype,
                code=400,
                version=PATCH_VERSION,
            )

        if starttime is not None and not Args.isValidTime(starttime):
            return error_request(
                msg=Error._400_,
                msg_detail=Error.INVL_TIME + starttime,
                code=400,
                version=PATCH_VERSION,
            )
        if endtime is not None and not Args.isValidTime(endtime):
            return error_request(
                msg=Error._400_,
                msg_detail=Error.INVL_TIME + endtime,
                code=400,
                version=PATCH_VERSION,
            )
        if starttime is not None and endtime is not None and starttime > endtime:
            starttime = starttime.replace("T", " ")
            endtime = endtime.replace("T", " ")
            return error_request(
                msg=Error._400_,
                msg_detail=Error.START_LATER + starttime + " > " + endtime,
                code=400,
                version=PATCH_VERSION,
            )
        tablename = ALL_RECORDS if datatype == VALIDATED else BUD_RECORDS

        result = current_app.db_Orphanfile.getOrphanfile(
            tablename, networks, station, channels, starttime, endtime, location
        )

        if type(result) is dict:
            if result["code"] == 400:
                return error_request(
                    msg=Error._400_,
                    msg_detail=result["msg"],
                    code=result["code"],
                    version=PATCH_VERSION,
                )
            elif result["code"] == 500:
                return error_request(
                    msg=Error._500_,
                    msg_detail=result["msg"],
                    code=result["code"],
                    version=PATCH_VERSION,
                )
            else:
                return error_request(
                    msg=result["msg"], code=result["code"], version=PATCH_VERSION
                )

        # Analyse len of results
        code = response_code(result)
        if code != 200:
            return code

        keys = ["filename"]
        json_result = tupleTojson(keys, result)

        if format == "text":

            header = " # filename"
            result_text = tupleTotext(keys, result, header)
            current_app.api.representations = {"*/*": general}
            return result_text

        else:
            current_app.api.representations = {"application/json": output_json}
            logger_Orphanfile.debug(" result will be returned")
            return json_result

    except Exception as e:
        logger_Orphanfile.exception(str(e))
        return error_request(msg=Error.PROCESSING, code=500, version=PATCH_VERSION)
