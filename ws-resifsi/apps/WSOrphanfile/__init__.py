"""
  Create Orphanfile  namespace with  its resources.
"""

from flask_restplus import Namespace
from .resources import *
from .__version__ import MAJOR_VERSION

orphanfile_ns = Namespace("orphanfile", description="Orphanfile description")

orphanfile_ns.add_resource(Orphanfile, "/" + MAJOR_VERSION + "/query")
orphanfile_ns.add_resource(Version, f"/{MAJOR_VERSION}/version")
orphanfile_ns.add_resource(Application, f"/{MAJOR_VERSION}/application.wadl")
orphanfile_ns.add_resource(Docs_orphanfile, f"/{MAJOR_VERSION}/", "/")
