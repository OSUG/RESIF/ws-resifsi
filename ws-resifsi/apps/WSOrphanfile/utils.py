"""
Contain all global variables and functions to check  parameters errors.

"""
from ..extensions.UtilValidate import CheckArg

VALIDATED = "validated"
RAW = "raw"
DATATYPE_LIST = [VALIDATED, RAW]
MAX_ARGUMENTS = 8
VALID_ARGUMENTS = [
    "network",
    "datatype",
    "format",
    "station",
    "starttime",
    "endtime",
    "location",
    "channel",
]
ALL_RECORDS = "rall"
BUD_RECORDS = "rbud"
TIMEOUT = "5000"
FORMAT_LIST = ["text", "json", None]


class CheckArgOrphanfile(CheckArg):
    def isMaxLen(listArgs, maxA):
        return len(listArgs) > maxA

    def isValidDatatype(datatype):
        return datatype in DATATYPE_LIST

    def isValidFormat(format):
        return format in FORMAT_LIST
