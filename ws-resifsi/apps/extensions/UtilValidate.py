"""
  Classe conatain functions to check common parameters values.
  Others classes are created to check specific parameters WS in each utils.py.

"""
import re
from datetime import datetime


class CheckArg(object):
    def isValid(listArgs, ValidList):

        for arg in listArgs:
            if arg not in ValidList:
                return False, arg
        return True, ""

    def isValidNetwork(network):
        return True if re.match("^[A-Za-z0-9\?]{1,2}$", network) else False

    def isValidStation(station):
        return True if re.match("^[A-Za-z0-9\?]{1,5}$", station) else False

    def isValidLocation(location):
        return True if re.match("^[A-Za-z0-9]{1,2}$", location) else False

    def isValidChannel(channel):
        return True if re.match("^[A-Za-z0-9\?]{1,3}$", channel) else False

    def isFormatDate(date, format):

        try:

            date = datetime.strptime(date, format)
            return True

        except Exception as e:
            return False

    def isValidTime(mydate):

        ok = (
            CheckArg.isFormatDate(mydate, "%Y-%m-%d")
            or CheckArg.isFormatDate(mydate, "%Y-%m-%dT%H:%M:%S")
            or CheckArg.isFormatDate(mydate, "%Y-%m-%dT%H:%M:%S.%f")
        )
        return ok if mydate else False
