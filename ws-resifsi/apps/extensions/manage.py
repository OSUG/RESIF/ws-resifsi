import os
import psycopg2
from datetime import datetime
import logging


class DatabaseConnection(object):
    def __init__(self, database, user, password, host):

        """Connection  to database
        :param dbname:
        :param user:
        :param password:
        :param host:

        """
        try:

            self._conn = psycopg2.connect(
                f"dbname = {database} user =\
                {user} password = {password} host = {host} connect_timeout=6"
            )
            self._cur = self._conn.cursor()

            return {"conn": self._conn, "cur": self._cur}

        except Exception as e:
            logging.error(e)
            return str(e)

    def reconnect(self, database, user, password, host):

        """Reconnection to database. it is called when  connection session  is aborted.
        :param dbname:
        :param user:
        :param password:
        :param host:

        """

        try:
            self._conn = psycopg2.connect(
                f"dbname = {database} user =\
                    {user} password = {password} host = {host} connect_timeout=6"
            )
            self._cur = self._conn.cursor()

        except Exception as e:

            return f" Failed reconnection to {databse} with error: {str(e)}"

    def close_connection(self):
        """ Close connection """

        try:
            self._cur.close()
            self._conn.close()

        except Exception as e:
            return str(e)
