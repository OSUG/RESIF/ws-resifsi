from flask import Response, request
from datetime import datetime


class Error(object):

    """
    Contain all  error message

    """

    _400_ = "Some of your query parameters are invalid or non-coherent"
    _500_ = "Internal Server Error"
    _203_ = "You are requesting too much data in a single request. Please split your request in smaller ones"
    _201_ = "No data"

    # commen errors
    LEN_ARGS = " Too much arguments in URL."
    UNKNOWN_PARM = " Unknown query parameter "
    INVL_LEVEL = " Invalid level arguments value."
    INVL_NODE = " Invalid node code "
    INVL_NETWORK = " Invalid network code "
    INVL_TIME = " Bad date value "
    INVL_TYPE = " Invalid type "
    INVL_STATION = " Invalid station code "
    INVL_LOCATION = " Invalid location code "
    INVL_CHANNEL = " Invalid channel code "
    INVL_DATATYPE = " Invalid datatype "
    START_LATER = " Starttime cannot be later than endtime : "
    PROCESSING = " Error processing your request "
    BAD_ARGS = " You use arguments for transaction level "
    INVL_FORMAT = "   Unrecognized format: "

    # Transaction WS  errors
    TRANS_NOEXIST = " Transaction doesn't exist "
    TRANS_REQUIRED = " Transaction name is mandatory "
    INVL_STATUS = " Invalid transaction  status "
    INVL_TEST = " Invalid test code "
    INVL_TRANS = " Invalid transaction type "
    INVL_TRANS_ID = " Invalid transaction ID "

    # Orphanfile  WS  errors
    NETWORK_REQUIRED = " Network code is mandatory "

    # Assembleddata WS  errors
    LEN_ARGS_FORM1 = (
        " Les seuls parametres valides pour ce format de requette sont NAME et FORMAT. "
    )
    INVL_LATITUDE = " Bad latitude value "
    INVL_LONGITUDE = " Bad longitude value "
    INVL_DEPTH = " Bad depth value "
    BEFORE_AFTER = " Before time cannot be later than After time "
    MAX_MIN_LATITUDE = " minimum latitude cannot be larger than maximum latitude"
    MAX_MIN_LONGITUDE = " minimum longitude cannot be larger than maximum longitude"
    MAX_MIN_MAGNITUDE = " minimum magnitude cannot be larger than maximum magnitude"
    MAX_MIN_DEPTH = " minimum depth cannot be larger than maximum depth"
    REQUIRED_TIME = " before and after arguments  are  required "
    UNKNOWN_COLLECTOR = " UNKNOWN_COLLECTOR: "
    MAX = " The time period must be less than one month. Please check 'after' and 'before' values. "


def error_handler(message_error, code):

    """
    Construct the Response contain  error to  return to user
    param str message_error: the error message
    param int code : code error

    """
    resp = Response(message_error + " \n", status=code, mimetype="*/*")
    return resp


def error_request(msg=" ", msg_detail=" ", code=" ", version=" "):
    """
            Construct the Response contain  error to  return to user
    :param str msg: the error message
    :param str code: code error
    :param str msg_detail: the detail error message to help user.
    :param str verison: The webservice version
    """
    message_error = f"       Error {code}: {msg}. Please mention your request URL when asking for support.\n\n\
	More Details: {msg_detail}\n\n\
	Request: {request.url}\n\
	Request Submitted: {datetime.utcnow().strftime('%Y-%b-%d %H:%M:%S UTC')} \n\
	Service version: version {version}\n"

    return error_handler(message_error, code)
