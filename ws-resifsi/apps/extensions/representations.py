"""
	This module contains functions to define  other 
	type of representations. The default representation is Json
"""

from flask import make_response


def general(data, code, headers=None):
    """
    The methode es used to build TEXT representation reponse.
    :param str data: data to send
    :param str code: Response code
    :param str : The header of response
    """

    resp = make_response(data, code)
    resp.headers.extend(headers or {})
    return resp
