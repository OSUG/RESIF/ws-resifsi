#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The starting point of the webservice
 
 - initialize the flask application
 - initialize the api 
 - creation of loggers

"""
import os
import sys
from flask import current_app
from flask import Flask, render_template
import logging.config
from flask_restplus.representations import output_json
from flask_restplus import Api

import settings
from ReverseProxied import ReverseProxied
from apps.WSOrphanfile.models import ResifOrphanfile
from apps.WSAssembleddata.models import ResifAssembleddata
from apps.WSTransactions.models import ResifTransactionIntgaration


def create_application(filename_config):
    """
    Create flask application from settings.py file.

    :param str filename_config: file contains flask application's configuration.

    """

    app = Flask(__name__)
    if os.environ["RUNMODE"] == "production":
        app.wsgi_app = ReverseProxied(app.wsgi_app, script_name="/resifsi")
        app.config.from_object(filename_config.ProductionConfig)
    else:
        app.config.from_object(filename_config.DevelopmentConfig)
    context_app = app.app_context()
    context_app.push()
    return app


def create_api():
    """
    Create flask API and add namespaces to it.instance of flask_restplus Api.

    """
    from apps.WSTransactions import transaction_ns as transaction
    from apps.WSOrphanfile import orphanfile_ns as orphanfile
    from apps.WSAssembleddata import assembleddata_ns as assembleddata

    api = Api(doc=False)
    api.add_namespace(transaction)
    api.add_namespace(assembleddata)
    api.add_namespace(orphanfile)

    return api


def connection_to_DB():
    """
    Connect each Web Service to its database. This connections instances are added to current_app.

    """
    current_app.db_Orphanfile = ResifOrphanfile()
    current_app.db_Transaction = ResifTransactionIntgaration()
    current_app.db_Assembleddata = ResifAssembleddata()


def config_logger(app):
    """
    Configure logger application from file.ini

    """
    logging.basicConfig()
    if os.getenv("RUNMODE") == "production":
        logging.getLogger().setLevel(logging.INFO)
    else:
        logging.getLogger().setLevel(logging.DEBUG)



app = create_application(settings)
config_logger(app)
logger_root = logging.getLogger("root")
app.logger.debug(" Creation flask application ")


@app.route("/")
def index():
    """
    It returns RESIFSI webervices home page: templates/indexWS.html.
    """
    return render_template("indexWS.html")


api = create_api()
api.init_app(app)
app.api = api
logger_root.debug(" flask-RestPlus initialized ")

if "RUNMODE" in os.environ:
    logger_root.debug(" Connexion to DB ")
    connection_to_DB()


if __name__ == "__main__":

    current_app.run(host="0.0.0.0")
