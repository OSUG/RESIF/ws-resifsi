.. ws-resifsi documentation master file, created by
   sphinx-quickstart on Fri Feb  1 09:02:32 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ws-resifsi's documentation!
======================================

.. toctree::
   :maxdepth: 1


   apps
   tests
   api
   ReversePrxied
   settings
   requirements
   README

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
