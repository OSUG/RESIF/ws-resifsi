import os
import sys

os.environ['RUNMODE']='production'
os.environ['WEBSERVICES_APP']='/var/www/html/ws-resifsi'


##Add this file path to sys.path in order to import settings
sys.path.insert(0, os.environ["WEBSERVICES_APP"])


##Create appilcation for our app
from api import app as application


