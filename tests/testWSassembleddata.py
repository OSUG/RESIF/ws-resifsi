"""
 un ensemble de requete spour tester les 
"""


def test_no_data(client):
    """Summary

    Args:
        client (flask ): create in conftest.py
    """
    result = client.get("/assembleddata/1/query?name=test&format=text")
    assert result.status_code == 201


def test_get_archive_info(client):
    """test  get archive by name

    Args:
        client (TYPE): Description
    """

    HEADERS = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "*/*;charset=utf-8",
    }

    result = client.get(
        "/assembleddata/1/query?name=2017.10.24T20:47:11&format=text", headers=HEADERS
    )
    assert result.status_code == 200


def test_get_archive(client):
    """test  get archive by name

    Args:
        client (TYPE): Description
    """

    HEADERS = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "application/zip ;charset=utf-8",
    }

    result = client.get(
        "/assembleddata/1/query?name=2017.10.24T20:47:11&format=archive",
        headers=HEADERS,
    )
    assert result.headers["content-type"] == "application/zip"
    assert result.status_code == 200


def test_get_archive_info_errorHedaers(client):
    """test  get archive by name
    get format text but send json in headers

    Args:
        client (TYPE): Description
    """

    HEADERS = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "application/json;charset=utf-8",
    }

    result = client.get(
        "/assembleddata/1/query?name=2017.10.24T20:47:11&format=text", headers=HEADERS
    )
    assert result.status_code == 500


def test_bad_formattime(client):
    """Summary

    Args:
        client (TYPE): Description
    """
    HEADERS = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "*/*;charset=utf-8",
    }

    result = client.get(
        "/assembleddata/1/query?before=2017-01&after=2016-05-01&minlatitude=12&maxlatitude=80&format=text",
        headers=HEADERS,
    )
    assert result.status_code == 400


def test_bad_latitude(client):
    """Summary

    Args:
        client (TYPE): Description
    """
    HEADERS = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "*/*;charset=utf-8",
    }

    result = client.get(
        "/assembleddata/1/query?before=2017-05-12&after=2017-05-01&minlatitude=120&maxlatitude=80&format=text",
        headers=HEADERS,
    )
    assert result.status_code == 400

    result = client.get(
        "/assembleddata/1/query?before=2017-05-12&after=2017-05-01&minlatitude=90&maxlatitude=80&format=text",
        headers=HEADERS,
    )
    assert result.status_code == 400


def test_bad_longitude(client):
    """Summary

    Args:
        client (TYPE): Description
    """
    HEADERS = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "*/*;charset=utf-8",
    }

    result = client.get(
        "/assembleddata/1/query?before=2016-01-12&after=2016-05-01&minlongitude=190&maxlongitude=80&format=text",
        headers=HEADERS,
    )
    assert result.status_code == 400

    result = client.get(
        "/assembleddata/1/query?before=2016-01-12&after=2016-05-01&minlongitude=120&maxlongitude=80&format=text",
        headers=HEADERS,
    )
    assert result.status_code == 400


def test_bad_magnitude(client):
    """Summary

    Args:
        client (TYPE): Description
    """
    HEADERS = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "*/*;charset=utf-8",
    }

    result = client.get(
        "/assembleddata/1/query?before=2016-05-12&after=2016-05-01&minmagnitude=190&maxmagnitude=-80&format=text",
        headers=HEADERS,
    )
    assert result.status_code == 400


"""
def test_bad_depth(client):
	
	HEADERS = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "*/*;charset=utf-8",
    }

    result = client.get(
        "/assembleddata/1/query?before=2016-05-12&after=2016-05-01&mindepth=-120&maxdepth=80&format=text",
        headers=HEADERS,
    )
    assert result.status_code == 400

    result = client.get(
        "/assembleddata/1/query?before=2016-05-12&after=2016-05-01&minlatitude=-2&maxlatitude=-3&format=text",
        headers=HEADERS,
    )
    assert result.status_code == 400
"""


def test_unknow_parameters(client):
    """Summary

    Args:
        client (TYPE): Description
    """
    HEADERS = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "*/*;charset=utf-8",
    }

    result = client.get("/assembleddata/1/query?toto=12", headers=HEADERS)
    assert result.status_code == 400
