"""
 un ensemble de requete spour tester les 
"""


def test_no_data(client):
    """Summary

    Args:
        client (flask ): create in conftest.py
    """
    result = client.get("/transaction/1/query?transactionid=TESTNODT&format=text")
    assert result.status_code == 201


def test_404(client):

    result = client.get("/fausseURL/1/query?idtransaction=testnotdata")
    assert result.status_code == 404


def test_faux_parametre(client):

    result = client.get("/transaction/1/query?toto=test")
    assert result.status_code == 400


# Test check arguments
def test_startime_endtime(client):

    result = client.get(
        "/transaction/1/query?starttime=2017-8-01&endtime=2017-8-02T23:04:30"
    )
    assert result.status_code == 200

    result = client.get(
        "/transaction/1/query?starttime=2017-8-01&endtime=2017-8-02:23:04:30"
    )
    assert result.status_code == 400


def test_node_network(client):

    result = client.get("/transaction/1/query?node=RAP&network=RA&starttime=2016-11-25")
    assert result.status_code == 200


def test_status_transactionid(client):

    result = client.get("/transaction/1/query?status=128")
    assert result.status_code == 200

    result = client.get("/transaction/1/query?status=129")
    assert result.status_code == 400

    result = client.get("/transaction/1/query?transactionid=@test")
    assert result.status_code == 400


def test_erroron_format(client):

    result = client.get("/transaction/1/query?erroron=T19")
    assert result.status_code == 400

    result = client.get("/transaction/1/query?format=toto")
    assert result.status_code == 400
