def test_no_data(client):
    """Summary

    Args:
        client (flask ): create in conftest.py
    """
    result = client.get("/orphanfile/1/query?network=TT")
    assert result.status_code == 201


def test_succes(client):

    result = client.get(
        "/orphanfile/1/query?network=FR&starttime=2018-11-20&format=json"
    )
    assert result.status_code == 200


def test_bad_time(client):

    result = client.get("/orphanfile/1/query?network=FR&starttime=2018")
    assert result.status_code == 400
    assert "Bad date value" in str(result.get_data())


# Test check arguments
def test_startime_endtime(client):

    result = client.get(
        "/orphanfile/1/query?network=FR&starttime=2016-8-01&endtime=2016-8-02T23:04:30"
    )
    assert result.status_code == 200


def test_network(client):
    result = client.get("/orphanfile/1/query?network=FR,RA")
    assert result.status_code == 200

    result = client.get("/orphanfile/1/query?network=FRRA")
    assert result.status_code == 400


def test_channel(client):

    result = client.get("/orphanfile/1/query?network=MT&channel=HHE")
    assert result.status_code == 200

    result = client.get("/orphanfile/1/query?network=MT&channel=HHE,YUJ")
    assert result.status_code == 200


def test_location(client):

    result = client.get("/orphanfile/1/query?network=MT&location=001")
    assert result.status_code == 400

    result = client.get("/orphanfile/1/query?network=MT&location=00,01")
    assert result.status_code == 200


def test_datatype(client):

    result = client.get("/orphanfile/1/query?network=MT&datatype=test")
    assert result.status_code == 400

    result = client.get("/orphanfile/1/query?network=MT&datatype=raw")
    assert result.status_code == 200
