
ce dépot contient ls codes sources des webservices dites SI. 
 

## Documentation 

### Configuration

La config se fait maintenant par variables d'environnements

  * RUNMODE : permet de choisir production ou autre
  * Webservice Transaction :
    * TRANSACTION_PGHOST=resif-pgprod.u-ga.fr 
    * TRANSACTION_PGUSER=resifintegration 
    * TRANSACTION_PGDATABASE=resifintegration 
    * TRANSACTION_PGPASSWORD=____
    * TRANSACTION_DIR=/data : c'est le dossier contenant tous les fichiers XML de transactions
  * Webservices Orphanfiles et Assembleddata
    * PGHOST=resif-pgpreprod.u-ga.fr 
    * PGUSER=resifinv_ro
    * PGDATABASE=resifInv-Preprod *
    * PGPASSWORD=______

### Prérequis de déploiement

Accès aux bases de données resifInv-Prod et resifintegration. L'utilisateur doit avoir les permissions suivantes :

  * Webservice transaction :
    * `GRANT CONNECT ON DATABASE resifintegration TO wsresifsi`
    * `GRANT SELECT ON all tables IN SCHEMA public TO wsresifsi`
  * Webservice Orphanfiles :
    * `GRANT CONNECT ON DATABASE "resifInv-Prod" TO wsresifsi`
    * `GRANT SELECT ON rbud TO wsresifsi`
    * `GRANT SELECT ON rall TO wsresifsi`
  * Webservice AssembledData :
    * `GRANT CONNECT ON DATABASE ResifInv-Prod TO wsresifsi`
    * `GRANT SELECT ON assembleddata TO wsresifsi`


### Pour les utilisateurs:

    ws.resif.fr/resifsi/

### Pour les Développeurs

#### Schéma général de dépot:
```
ws-resifsi:
|
|------apps :  # Contient les src des webservices
|      |
|      |----extensions : # modules interne
|      |        |
|      |        |---__init__.py 
|      |        |---manage.py   # j'ai mit une classe génarale pour la connexion et excuter une requete 
|      |        |                chaque WS cotiendra sa propre classe de base de données qui hérite de cette classe
|      |        |---UtilErrors   # rassemble les erreurs
|      |        |---UtilValidate  # fonction pour valider les valeurs des arguments en entrée
|      |
|      |----WS_Noms_de_webservice : # l'implémentation de chaque WS suivras le même shéma
|      |        |
|      |        |---__init__.py    on definit le namespace et ses resources (les urls)
|      |        |---parameters.py  contient les parametres de connexion
|      |        |---resources.py   dans ce fichier on détailleras la resources ( parametres + appel au traitement)
|      |        |---lib.py         contient le traitement de chaque resource
|      |        |---models.py      contient la classe qui fait appel a la base de données de WS
|      |
|      |-----__init___.py  on initialise le webeservice et on rajoute les namespaces ( namespace = WS) 
|-----docs   documentation  pour les développeurs , généréer par sphinx
|-----static : contient les fichiers CSS 
|-----templates:  
|-----api.py   Initiliser l'application
|-----.gitignore   
|-----requirements 
|-----settings : variable de config flask selon RUNMODE (test ou prod)
```

#### Détail des modules

  La documentation peut etre  généré automatiquement avec sphinx. 
  

  Pour générer les .rst:  
```sphinx-apidoc  -f -e -o docs/source  . ```

  Pour générer les html:  
```sphinx-build  docs/source/ docs/build/html ```

  Ainsi la documentation sera généré dans ws-resifsi/docs/build/html



### Tests
Des tests fonctionnels sont ecrits dans le dossiers tests. Vous pouvez les lancer avec :
```
pytest tests/testWS*
```
